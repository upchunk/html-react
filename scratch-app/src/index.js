import React from "react";
import ReactDOM from "react-dom";
import App from "./App";


var mountNode = document.getElementById("app");
var postsPerPage = mountNode.dataset.postsPerPage;
postsPerPage = postsPerPage ? Number(postsPerPage) : 10;
ReactDOM.render(<App postsPerPage={postsPerPage} />, mountNode);