import React from 'react';
import Avatar from './avatar';

export default {
    title: 'Avatar'
};

export const propless = () => (<Avatar userId={69} />);