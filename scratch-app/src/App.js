import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Posts from './posts';
import styled from 'styled-components';
import ItemsForm from './items-form';

const OuterBox = styled.div`
    background-color: whitesmoke;
    border: 1px solid lightgrey;
    border-radius: 10px;
    padding: 20px;
    margin: 0 15px;
`

const App = ({ postsPerPage, changeLimitHandler }) => {
  const [postsPerPageState, setPostsPerPageState] = useState(postsPerPage);
  return (<OuterBox>
    <ItemsForm
      changeLimitHandler={val => {
        setPostsPerPageState(val);
      }}
      initialLimitValue={postsPerPageState}
    />
    <Posts
      postsPerPage={postsPerPageState}
    />
  </OuterBox>)
};

App.propTypes = {
  postsPerPage: PropTypes.number.isRequired
};

export default App;