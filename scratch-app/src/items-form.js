import React from 'react';
import PropTypes from 'prop-types';

const ItemsForm = ({ changeLimitHandler, initialLimitValue }) => {
    return (<div>
        <label htmlFor="num-items" >Max Items: </label>
        <input
            name="num-items"
            defaultValue={initialLimitValue}
            onChange={ev => changeLimitHandler(Number(ev.target.value) || 1)}
        />
    </div>)
};

ItemsForm.propTypes = {
    changeLimitHandler: PropTypes.func.isRequired,
    initialLimitValue: PropTypes.number.isRequired
};

export default ItemsForm;