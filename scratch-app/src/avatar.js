import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const names = `
Wade
Dave
Seth
Ivan
Riley
Gilbert
Jorge
Dan
Brian
Roberto
Ramon
Miles
Liam
Nathaniel
Ethan
Lewis
Milton
Claude
Joshua
Glen
Harvey
Blake
Antonio
Connor
Julian
Aidan
Harold
Conner
Peter
Hunter
Eli
Alberto
Carlos
Shane
Aaron
Marlin
Paul
Ricardo
Hector
Alexis
Adrian
Kingston
Douglas
Gerald
Joey
Johnny
Charlie
Scott
Martin
Tristin
Troy
Tommy
Rick
Victor
Jessie
Neil
Ted
Nick
Wiley
Morris
Clark
Stuart
Orlando
Keith
Marion
Marshall
Noel
Everett
Romeo
Sebastian
Stefan
Robin
Clarence
Sandy
Ernest
Samuel
Benjamin
Luka
Fred
Albert
Greyson
Terry
Cedric
`.split('\n').filter(name => name)

const PlainCircle = styled.div`
    border-radius: 38px;
    width: 50px;
    height: 50px;
    position: relative;
    background-color: lightcoral;
    text-align: center;
`

const FancyCharacter = styled.span`
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    font-size: 2em;
    color: whitesmoke;
`

const Avatar = ({ userId, style }) => {
    return (<PlainCircle style={style}>
        <FancyCharacter>
            {/* first letter */}
            {names[Number(userId)].charAt(0)}
        </FancyCharacter>
    </PlainCircle>)
};

Avatar.propTypes = {};

export default Avatar;