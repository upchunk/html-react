import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Post from './post';

const Posts = ({ postsPerPage }) => {
    const [posts, setPosts] = useState([]);
    const [postsPerPageState, setPostsPerPageState] = useState(postsPerPage);
    useEffect(() => {
        axios({
            headers: {},
            method: 'get',
            url: 'https://jsonplaceholder.typicode.com/posts',
            responseType: 'json'
        })
            .then(response => setPosts(response.data));
    }, [])
    useEffect(() => {
        setPostsPerPageState(postsPerPage);
    })
    const subset = posts.slice(0, postsPerPageState);
    return (<>
        {subset.map((post, i) => <Post post={post} key={i} />)}
    </>)
};


export default Posts;