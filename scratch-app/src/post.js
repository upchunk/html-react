import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Avatar from './avatar';

const Cols = styled.div`
    display: flex;
    flex-direction: row;
`

const Post = ({ post }) => {
    return (<Cols>
        <div>
            <Avatar
                style={{ marginRight: '10px' }}
                userId={post.userId}
            />
        </div><div>
            <h2>{post.title}</h2>
            <div>{post.body}</div>
        </div>
    </Cols>)
};

Post.propTypes = {};

export default Post;