FROM node:10.13-alpine as build
WORKDIR /usr/src/app
COPY scratch-app .
RUN npm i
RUN npm run build-prod

FROM nginx:1.13.9-alpine
WORKDIR /usr/share/nginx/html
COPY html5up-stellar .
COPY --from=build /usr/src/app/dist ./dist
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]