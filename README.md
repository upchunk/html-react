# HTML API Fetch with React

### Why this?
Add an interactive application into a pre-existing HTML document.

There are two pieces to this;
* A React application at `scratch-app` which will run by itself, but is meant to be added to an HTML page.
* A static HTML Page at `html5up-stellar`.

To avoid overly complicating the demo;
* fetch results are assumed to resolve.
* Some components, such as the posts parts use the CSS of the template. This is by design.
* The only interactive part of the application is that a number can be typed into an input to change the amount of results displayed.
* All state is stored in the individual components -- not in Redux, XState, or even sessionStorage.
* An [HTML-Up](https://html5up.net/stellar) template was used as an example. Any other pre-existing HTML document could be used.
* Uses [JSONPlaceholder](https://jsonplaceholder.typicode.com/) for fake posts data.
* Uses [Axios](https://github.com/axios/axios) to get JSON data.
* Uses [Styled-Components](https://github.com/styled-components/styled-components) for uhh... styling components.
* Uses [Parcel-Bundler](https://parceljs.org/) to convert to ES5.

In order to insert the React application, an `app` section was added to the template.
Though I could have used JavaScript to insert the element, there was a specific place I wanted the app to display in the page.

```
<!-- React App Section -->
<section id="app" data-posts-per-page="5"></section>
```

In addition, you would also manually add the location of the built assets.
```
<script src="dist/index.js"></script>
```


Note:
React applications are just that -- applications.
They would normally be the entry points to the site, not static HTML.
With that said, the demo here is more a React *plugin* -- added functionality to a pre-existing site.
Rather than building the app locally and manually placing the code into the HTML page, it could easily reside in the cloud.
If you were to serve static HTML pages (e.g., an `index.html` and `about.html`), each page would have to load the application; this is also the case of SSG, which serve static components, then load JS.


What you can do here
* Go to [htmlreact.computercarl.com](https://htmlreact.computercarl.com) to see the demo.
* The repo includes a multi-stage Dockerfile, so you can see the result without using locally.
* `npm run storybook` -- isolated component development
* `npm run` -- will run a parcel-bundler server, but fetching files while attempting to use `localhost` will most likely cause a CORS error. 